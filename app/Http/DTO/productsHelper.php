<?php
/**
 * Created by PhpStorm.
 * User: Alaa
 * Date: 8/14/2018
 * Time: 6:43 PM
 */

namespace App\Http\DTO;

use Validator;


class productsHelper
{
    public static function prepareCreate($data) {
        $output = array();
        $output['product_name'] = $data->n;
        $output['quantity'] = $data->s;
        $output['price'] = $data->p;
        return $output;
    }
    public static function allProducts($result) {
        $output = array();
        foreach ($result as $key=>$value) {
            $output[$key] = productsHelper::singleProduct($value);
        }
        return $output;
    }
    public static function singleProduct($data) {
        $output = array();
        $output['name'] = $data->product_name;
        $output['quantity'] = $data->quantity;
        $output['price'] = $data->price;
        $output['time'] = $data->created_at;
        $output['total'] = $data->quantity*$data->price;
        return $output;
    }
}