<?php

namespace App\Http\Controllers;

use App\Http\DTO\productsHelper;
use App\Http\Services\productsServices;
use Illuminate\Http\Request;
use App\Http\Repository\productsRepository;

class productController extends Controller
{
    private $productsRepository;
    private $productServices;

    function __construct(productsRepository $productsRepository,productsServices $productsServices)
    {
        $this->productsRepository = $productsRepository;
        $this->productServices = $productsServices;
    }

    function setProduct(Request $request)
    {
        $validator = $this->productServices->setValidation($request);
        if ($validator->fails())
            return response()->json($validator->errors()->all(),404);
        else {
            $data = productsHelper::prepareCreate($request);
            $data = $this->productsRepository->create($data);
            $data = productsHelper::singleProduct($data);
            return response()->json($data);
        }
    }
    function updateProduct(Request $request,$id)
    {
        $product = @$this->productsRepository->findWhere(['id'=>$id]);

        if (count($product) == 0)
            return response()->json(['error' => 'the product not exist at our database'], 404);
        else {
            $validator = $this->productServices->updateValidation($request,$id);
            if ($validator->fails())
                return response()->json($validator->errors()->all(), 404);
            else {
                $data = productsHelper::prepareCreate($request);
                $data = $this->productsRepository->update($data, $id);
                $data = productsHelper::singleProduct($data);
                return response()->json($data);
            }
        }
    }
    function getProducts()
    {
        $all = $this->productsRepository->all();
        $all = productsHelper::allProducts($all);
        return response()->json($all);
    }
    function deleteProduct($id) {
        $product = @$this->productsRepository->findWhere(['id'=>$id]);

        if (count($product) == 0)
            return response()->json(['error' => 'the product not exist at our database'], 404);
        else {
            $this->productsRepository->delete($id);
            return response(['message' => 'The item deleted']);
        }
    }
}
