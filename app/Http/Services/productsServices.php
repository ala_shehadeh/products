<?php
/**
 * Created by PhpStorm.
 * User: alaa.shehadeh
 * Date: 9/4/2018
 * Time: 10:34 AM
 */

namespace App\Http\Services;

use Validator;


class productsServices
{
    public function setValidation($request) {
        $rules = [
            'n' => 'required|unique:products,product_name|min:3',
            's' => 'required|numeric|min:1',
            'p' => 'required|numeric|min:0'
        ];
        $input = $request->only(
            'n',
            's',
            'p'
        );
        $messages = [
            'n.required|unique:products,product_name|min:3' => 'The product name is required and minmum three character',
            's.required|numeric|min:1' => 'The product price required and should be in integer value',
            'p.required|numeric|min:0' => 'The product price required and should be in integer value',
        ];
        $validator = Validator::make($input, $rules,$messages);
        return $validator;
    }
    public function updateValidation($request,$id) {
        $rules = [
            'n' => 'required|min:3|unique:products,product_name,'.$id,
            's' => 'required|numeric|min:1',
            'p' => 'required|numeric|min:0'
        ];
        $input = $request->only(
            'n',
            's',
            'p'
        );
        $messages = [
            'n.required|unique:products,product_name|min:3' => 'The product name is required and minmum three character',
            's.required|numeric|min:1' => 'The product price required and should be in integer value',
            'p.required|numeric|min:0' => 'The product price required and should be in integer value',
        ];
        $validator = Validator::make($input, $rules,$messages);
        return $validator;
    }
}