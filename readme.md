its a simple lumen application mske RESTful API CRUD request to product table.

###Requirments 
to run the application the server/pc has to have the following:

* php 7
* Composer
* Mysql Database
* GIT

###setup

* run the following git request on cmd : 

git clone https://ala_shehadeh@bitbucket.org/ala_shehadeh/products.git
* cd product
* composer update to install the required dependencies
* rename .env.example to .env and fill your database host, name, user name and password
* run the following command to install the database: php artisan migrate
* run the following command to run the server: php -S localhost:3200 -t public

then you are done, on the root folder there is a json postman app to can test the application

the classes structure exist on /app/Http and the model exist on /app/products.php